
import 'phaser';
import {Stage} from "./stage";

let config = {
    type: Phaser.AUTO,
    width: 480,
    height: 270,
    scale: {
        mode: Phaser.Scale.FIT,
        autoCenter: Phaser.Scale.CENTER_BOTH,
        zoom: 3,
    },
    physics: {
        default: 'arcade',
        arcade: {
            gravity: {y: 0},
            debug: false,
        }
    },
    backgroundColor: '#000000',
    scene: [ Stage ]
};


window.addEventListener('load', () => {
    let game = new Phaser.Game(config);
});