import 'phaser'

export class Player extends Phaser.GameObjects.Graphics {

    constructor(scene, options) {
        super(scene, options);
        this.rotation = 0;
        this.rotationPerSecond = 1.66*Math.PI;
        this.speed = 0;
        this.maxSpeed = 100;
        this.accel = 100;

        this.left = scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.LEFT);
        this.right = scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.RIGHT);
    }

    addedToScene() {
        super.addedToScene();
        this.lineStyle(1, 0xFFFFFF, 1.0);
        this.fillStyle(0xFFFFFF, 1.0);
        this.strokeCircle(0, 0, 10);
        this.lineBetween(0, 0, 20, 0);

        this.body.setCircle(10, -10, -10);
    }

    update(ts, delta) {
        super.update(ts, delta);
        let dt = delta / 1000;

        if(this.left.isDown) this.rotation -= this.rotationPerSecond * dt;
        if(this.right.isDown) this.rotation += this.rotationPerSecond * dt;

        this.speed = Math.min(this.maxSpeed, this.speed + this.accel * dt)
        this.scene.physics.velocityFromRotation(
            this.rotation,
            this.speed,
            this.body.velocity,
        )
    }
}