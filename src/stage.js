import 'phaser'
import {Player} from "./player";

export class Stage extends Phaser.Scene {

    create() {
        this.player = new Player(this, {
            x: this.cameras.main.centerX,
            y: this.cameras.main.centerY
        });
        this.physics.add.existing(this.player);
        this.add.existing(this.player);

        this.customGroup = this.add.group({
            runChildUpdate: true,
        })
        this.customGroup.add(this.player);

        let f3 = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.F3);
        f3.on('down', () => {
            this.player.destroy();
        });
    }
}