# Netpath

This tutorial is adapted from and inspired by the excellent 
[BYTEPATH](https://github.com/a327ex/BYTEPATH).  In this version, we'll be
building Bytepath as a web game, using Typescript and Phaser.

This tutorial will attempt to be as faithful to the source material as
possible, which means a few things:

* It won't focus on over-engineering things; the Bytepath tutorial has an
  ethos of simply getting things done and this tutorial will reflect that.
* We may not be doing everything the 'Phaser' way.  Phaser is somewhat more
  opinionated than the LÖVE framework, in that LÖVE expects you to e.g.
  track all your sprites and objects whereas Phaser expects to own the
  objects and have you more generally describe their behavior.
  * I will include notes on whether or not we're using built-in Phaser
    functionality vs rolling it ourselves.  When possible, I'll include
    some details on how Phaser would do something we're doing manually.
  * The original tutorial stressed trying things out your own way and really 
    engaging with the content.  While that's entirely possible here (and I 
    encourage you to play around with what I've written here), Phaser's 
    opinionatedness often means that there's really only one way to do it.  


# What you need to know beforehand

* You'll need to be at least somewhat proficient in modern Javascript.
  The original Bytepath tutorial covers a lot of Lua-related material because
  things like Object orientation aren't directly built in.  This tutorial
  will assume you are already familiar with the concepts.

# Setup

Setting up Phaser (and web game development) is a bit more involved than LÖVE;
LÖVE is a self-contained ecosystem, whereas Phaser expects to integrate with
Javascript and web development as a whole.  To set up Phaser, you'll need:

* A build system: I'm using [Vite](https://vitejs.dev/).  This isn't just to
  compile the Typescript but also to put all the separate files we'll be
  using into one file that can be deployed.  Whatever system you choose,
  I'd highly recommend having it in 'watch' mode to automatically run
  whenever you make a change.
* A local web server (see [the phaser docs](https://phaser.io/tutorials/getting-started-phaser3)
  for why); note that if you're using Vite it has one built in. 

This repository has a `package.json` and `vite.config.js` that should work
for development purposes out of the box.